package rs.etf.zv163170m.activityrecognition;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import rs.etf.zv163170m.humanactivityrecognition.HumanActivityRecognizer;
import rs.etf.zv163170m.humanactivityrecognition.HumanActivityState;
import rs.etf.zv163170m.humanactivityrecognition.IHumanActivityChangedListener;

public class MainActivity extends AppCompatActivity implements IHumanActivityChangedListener, TextToSpeech.OnInitListener {

    private static final String TAG = "MainActivity";
    private static final int N_SAMPLES = 200;
    private static List<Float> x;
    private static List<Float> y;
    private static List<Float> z;
    private HumanActivityRecognizer humanActivityRecognizer;
    private TextView stateTextView;

    private TextToSpeech textToSpeech;
    private boolean textToSpeechInit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);
        x = new ArrayList<>();
        y = new ArrayList<>();
        z = new ArrayList<>();



        humanActivityRecognizer = new HumanActivityRecognizer(getApplicationContext(),this);

        stateTextView = (TextView) findViewById(R.id.state_label);

        textToSpeech = new TextToSpeech(this, this);
        textToSpeech.setLanguage(Locale.US);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        humanActivityRecognizer.stop();
    }

    @Override
    public void onInit(int status) {
        textToSpeechInit = true;
    }

    @Override
    public void onStateChanged(final HumanActivityState humanActivityState) {

        if(textToSpeechInit)
            textToSpeech.speak(humanActivityState.toString(), TextToSpeech.QUEUE_ADD, null, Integer.toString(new Random().nextInt()));


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                stateTextView.setText(humanActivityState.toString());
            }
        });


        Log.i(TAG, "New humanActivityState: " + humanActivityState );
    }
}
